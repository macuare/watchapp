/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package watchapp.service;

import java.text.DecimalFormat;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

/**
 *
 * @author Andy
 */
public class HerramientasServiceImpl implements HerramientasService{

    private AnchorPane ventana;
    
    @Override
    public void crearCirculo(double radio, int posX, int posY, String estilo) {
        Circle circulo = new Circle();
        circulo.setCenterX(posX);
        circulo.setCenterY(posY);
        circulo.setRadius(radio);
        circulo.setStyle(estilo);
        
        ventana.getChildren().add(circulo);
    }
    
    @Override
    public Rectangle crearRectangulo(double posX, double posY, double ancho, double alto, String estilo) {
        Rectangle r = new Rectangle();
        r.setX(posX);
        r.setY(posY-(alto/2));
        r.setWidth(ancho);
        r.setHeight(alto);
        
        Rotate rot = new Rotate(-90, posX, posY);
        r.getTransforms().add(rot);
        
        r.setStyle(estilo);
        
        ventana.getChildren().add(r);
        
        return r;
    }

    @Override
    public void setVentana(AnchorPane ventana) {
        this.ventana = ventana;
    }

    @Override
    public Label crearMarcador(double posX, double posY, String contenido) {
        Label m = new Label();
        
        m.setText(contenido);
        m.setPrefHeight(100);
        m.setPrefWidth(300);
        m.setLayoutX(posX);
        m.setLayoutY(posY);
        
        m.setStyle("-fx-font-family: \"Forte\";\n" +
                "    -fx-font-size: 82px;\n" +
                "    -fx-text-fill: white;    \n" +
                "    -fx-font-weight: bold;"
                + "  -fx-alignment: CENTER;"
                + "  -fx-effect:dropshadow( three-pass-box  ,white ,15 , 0.15 , 0.0 , 0.0 );");
        
        
        ventana.getChildren().add(m);
        
        return m;
    }

    @Override
    public String formatoNumero(int valor) {
        DecimalFormat df = new DecimalFormat("00");
        return df.format(valor);
    }
    

    
    
    
}
