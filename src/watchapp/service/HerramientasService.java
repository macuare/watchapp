/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package watchapp.service;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Andy
 */
public interface HerramientasService {
    
    public void setVentana(AnchorPane ventana);
    public void crearCirculo(double radio, int posX, int posY, String estilo);
    public Rectangle crearRectangulo(double posX, double posY, double ancho, double alto, String estilo);
    public Label crearMarcador(double posX, double posY,  String contenido);
    public String formatoNumero(int valor);
}
