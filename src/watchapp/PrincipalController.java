/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package watchapp;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import watchapp.service.HerramientasService;
import watchapp.service.HerramientasServiceImpl;

/**
 *
 * @author Andy
 */
public class PrincipalController implements Initializable {
    
    private HerramientasService herramientasService;
    
    @FXML
    private AnchorPane ventana;
   
     @FXML
    void CERRAR(ActionEvent event) {
        System.exit(0);
    }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ventana.setStyle("-fx-background-color: transparent;");
        
        herramientasService = new HerramientasServiceImpl();
        herramientasService.setVentana(ventana);
        
        Map<String, Rectangle> elementos = new HashMap();
        double ptoMedio = Math.round(ventana.getPrefWidth() / 2.0);
        double radio = (ventana.getPrefWidth() - 100)/2;
     
        
        // CREANDO FONDO
        herramientasService.crearCirculo(280 , 300 , 300, 
                "-fx-fill:  linear-gradient(to right, rgba(76,76,76,1) 0%, rgba(89,89,89,1) 12%, rgba(102,102,102,1) 25%, rgba(71,71,71,1) 39%, rgba(44,44,44,1) 50%, rgba(0,0,0,1) 51%, rgba(17,17,17,1) 60%, rgba(43,43,43,1) 76%, rgba(28,28,28,1) 91%, rgba(19,19,19,1) 100%); -fx-stroke: white;"
              + "-fx-effect:dropshadow( three-pass-box  ,blue ,15 , 0.15 , 0.0 , 0.0 );");
        
        // CREANDO MARCADOR
        Label marcador = herramientasService.crearMarcador(160, 200, LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        
        // CREANDO CIRCULOS SECUNDARIOS
        IntStream.iterate(0, i -> i + 30).limit(13).forEach(valor -> {
            int calculoY = (int)(Math.sin(Math.toRadians(valor)) * radio);
            int calculoX = (int)(Math.cos(Math.toRadians(valor)) * radio);
            System.out.println("Angulo: "+valor+" -> calculo: "+calculoX+" - "+calculoY);
            
            herramientasService.crearCirculo(radio/40, calculoX + (int)ptoMedio , calculoY + (int)ptoMedio, 
                    " -fx-fill: linear-gradient(to right, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%);"
                  + " -fx-effect:dropshadow( three-pass-box  ,white ,15 , 0.15 , 0.0 , 0.0 );");
        });
        
        // CREANDO AGUJA SEGUNDOS
            elementos.put("S", 
                herramientasService.crearRectangulo(ptoMedio, ptoMedio, 240, 5, 
                        " -fx-fill: linear-gradient(to right, rgba(210,255,82,1) 0%, rgba(145,232,66,1) 100%);"
                      + " -fx-effect:dropshadow( three-pass-box  ,#02ff27 ,15 , 0.15 , 0.0 , 0.0 );"
                      + " -fx-arc-height: 10px;"
                      + " -fx-arc-width: 80px;"));
        // CREANDO AGUJA MINUTOS
            elementos.put("M",
                herramientasService.crearRectangulo(ptoMedio, ptoMedio, 200, 7, 
                        " -fx-fill:  linear-gradient(to right, rgba(73,155,234,1) 0%, rgba(32,124,229,1) 100%);"
                      + " -fx-effect:dropshadow( three-pass-box  ,#33ccff ,15 , 0.15 , 0.0 , 0.0 );"
                       + " -fx-arc-height: 10px;"
                      + " -fx-arc-width: 80px;"));
        // CREANDO AGUJA HORAS
            elementos.put("H",
                herramientasService.crearRectangulo(ptoMedio, ptoMedio, 150, 10, 
                        " -fx-fill: linear-gradient(to right, rgba(203,96,179,1) 0%, rgba(193,70,161,1) 50%, rgba(168,0,119,1) 51%, rgba(219,54,164,1) 100%);"
                      + " -fx-effect:dropshadow( three-pass-box  ,#ff0202 ,15 , 0.15 , 0.0 , 0.0 );"
                + " -fx-arc-height: 10px;"
                      + " -fx-arc-width: 80px;"));
        
        // ROTACIONES
            ExecutorService proceso = Executors.newSingleThreadExecutor();
            proceso.submit(()->{
               
                double segundos = LocalDateTime.now().getSecond() * 6;
                double minutos = LocalDateTime.now().getMinute() * 6;
                double horas = LocalDateTime.now().getHour() * 15;
                
               
                // Condiciones Iniciales
                elementos.get("S").getTransforms().add(new Rotate(segundos, ptoMedio, ptoMedio));
                elementos.get("M").getTransforms().add(new Rotate(minutos, ptoMedio, ptoMedio));
                elementos.get("H").getTransforms().add(new Rotate(horas, ptoMedio, ptoMedio));
                
                
                while(true){
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        elementos.get("S").getTransforms().add(new Rotate(6, ptoMedio, ptoMedio));
                        segundos+=6;
                        
                        if(segundos == 360){
                            elementos.get("M").getTransforms().add(new Rotate(6, ptoMedio, ptoMedio));
                            segundos = 0; minutos += 6;
                        }
                        
                        if(minutos == 360){
                            elementos.get("H").getTransforms().add(new Rotate(horas, ptoMedio, ptoMedio));
                            minutos = 0;
                        }
                        
                        if(LocalDateTime.now().getSecond() % 2 == 0)
                         Platform.runLater(()->{
                             marcador.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                         });
                        else
                         Platform.runLater(()->{
                             marcador.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")).replace(":", " "));
                         });
                        
                    } catch (InterruptedException ex) {
                        Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                    
                
            });
                    
            
            
            
    }    
    
}
