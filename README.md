this is a Watch application developed in Java 8 and JavaFx 8
Time used: 4 hours

This desktop application uses FXML and CSS to show the operation of the digital clock.
It has several methods and implementations to build the visual interface and give it movement. It also makes use of different threads for background executions and also at a graphical level.
